import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class TestBook {
	
	private int inputbookprice;
	private double inputbookdiscount;
	private double expectedbookprice;
	private Book book;
	
	@Before
	public void initialize(){
		book = new Book();
	}
	
	public TestBook(int inputbookprice, double inputbookdiscount, double expectedbookprice){
		this.inputbookprice = inputbookprice;
		this.inputbookdiscount = inputbookdiscount;
		this.expectedbookprice = expectedbookprice;
	}
	
	@Parameterized.Parameters
	public static Collection<Object[]> bookprices() {
		return Arrays.asList(new Object[][] {
			{340,20,272},
			{345,20,276},
			{480,100,0},
			{500,10,400}
		});
	}
	
	@Test
	public void test() {
		
		System.out.println("Book price :"+inputbookprice);
		System.out.println("discount :"+ inputbookdiscount);
		System.out.println("Final price:"+ expectedbookprice);
		assertEquals(expectedbookprice,Book.discountedPrice(inputbookprice, inputbookdiscount),0.0);
		
	}

}
