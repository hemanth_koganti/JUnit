import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class DBTest {
	
	private static String drivername;
	private static String url;
	private static String uname;
	private static String pswrd;
	public String success = "Success";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		drivername="com.oracle.driver.OracleDriver";
		uname="system";
		pswrd="1234";
		url="jdbc:oracle:thin:@localhost:1521/XE";
	}

	@Test
	public void DBConnectionTest() {
		assertEquals(success,DBConnection.connect(drivername,url,uname,pswrd));
	}

}
