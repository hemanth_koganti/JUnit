import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class WordCount {
	
	public static void main(String[] args) throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String str = reader.readLine();
		int c = count(str);
		System.out.println(c);
	}
	
	public static int count(String str){
		String[] words = str.split("\\s+"); 
		return words.length;
	}

}
