import static org.junit.Assert.*;

import org.junit.Test;

public class TestWordCount {
	
	WordCount wordcount = new WordCount();
	
	String str = "My name is Hemanth";
	int count = wordcount.count(str);
	
	@Test
	public void testcount(){
		assertEquals(4,count);
	}

}
