import static org.junit.Assert.*;

import org.junit.Test;

public class ArraySortTest {
	
	int inputarray[] = {24,45,34,22,11};
	int expectedarray[] = {11,22,24,34,45};
	int returnedarray[] = ArraySort.arraysort(inputarray);

	@Test
	public void test() {
		assertArrayEquals(expectedarray,returnedarray);
	}

}
