import java.io.IOException;
import java.util.Scanner;

public class ArraySort {
	
	public static void main(String[] args) throws NumberFormatException, IOException{
		
		int inputarray[],resultarray[];
		Scanner s = new Scanner(System.in);
		System.out.print("Enter the no. of elements");
		int num = s.nextInt();
		inputarray = new int[num];
		System.out.print("Enter the elements");
		for(int i=0;i<num;i++){
			inputarray[i] = s.nextInt();
		}
		resultarray = new int[num];
		resultarray = arraysort(inputarray);
		for(int i=0;i<num;i++){
			System.out.println(resultarray[i]);
		}		
	}
	
	public static int[] arraysort(int[] arr){
		
		int temp;
		for(int i=0;i<arr.length-1;i++){
			for(int j=0;j<arr.length-i-1;j++){
				if(arr[j]>arr[j+1]){
					temp = arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;	
				}
			}
		}
		return arr;	
	}

}
